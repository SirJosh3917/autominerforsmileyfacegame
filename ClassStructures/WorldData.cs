﻿namespace genSendKeyPresses
{
	public static class WorldData
	{
		static WorldData() => Data = new uint[1, 1, 1];

		public static uint[,,] Data;
	}
}