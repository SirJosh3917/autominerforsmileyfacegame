﻿using System.Collections.Concurrent;
using System.Collections.Generic;

namespace genSendKeyPresses
{
	public class BlockDefinitions
	{
		public BlockDefinitions(int unknownMineAttempts)
		{
			_uma = unknownMineAttempts;
			_blocks = new List<Block>();
			_blockCache = new ConcurrentDictionary<uint, Block>();
		}

		private readonly int _uma;
		private List<Block> _blocks;
		private ConcurrentDictionary<uint, Block> _blockCache;

		public void AddBlock(Block b) => _blocks.Add(b);

		public Block Get(uint id)
		{
			if(_blockCache.TryGetValue(id, out var block))
			{
				return block;
			}

			foreach (var i in _blocks)
			{
				if (i.Id == id)
				{
					_blockCache.TryAdd(id, i);

					return i;
				}
			}

			block = new Block(Family.Unknown, id, _uma);

			_blockCache.TryAdd(id, block);

			return block;
		}
	}

	public static class BlockHelpers
	{
		public static BlockDefinitions Add(this BlockDefinitions definition, Block add)
		{
			definition.AddBlock(add);
			return definition;
		}

		public static BlockDefinitions Add(this BlockDefinitions definition, Family family, uint id, int mineAttempts)
		{
			definition.AddBlock(new Block(family, id, mineAttempts + 4));
			return definition;
		}
	}

	public class Block
	{
		public Block(Family family, uint id, int mineAttempts)
		{
			Family = family;
			Id = id;
			MineAttempts = mineAttempts;
		}

		public Family Family { get; }
		public uint Id { get; }
		public int MineAttempts { get; }

		public override string ToString()
			=> $"{Family}: '{Id}' (pow: {MineAttempts})";
	}

	public enum Family
	{
		Gravity,
		Dirt,
		Ore,
		Unknown,
		Lag
	}
}