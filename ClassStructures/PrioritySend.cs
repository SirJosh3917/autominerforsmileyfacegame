﻿using System;
using System.Collections.Concurrent;
using System.Threading;
using System.Threading.Tasks;

namespace genSendKeyPresses
{
	public class PrioritySend
	{
		public PrioritySend(Func<MessageInfo, Task> onItem)
		{
			_onItem = onItem ?? throw new ArgumentNullException(nameof(onItem));
			_queue = new ConcurrentQueue<MessageInfo>();
			_mres = new ManualResetEventSlim(false);
		}

		public async Task Run(CancellationToken ct)
		{
			while (!ct.IsCancellationRequested)
			{
				if (_queue.TryDequeue(out var item))
				{
					await _onItem(item);
				}
				else
				{
					_mres.Wait();
				}
			}
		}

		private readonly Func<MessageInfo, Task> _onItem;
		private ConcurrentQueue<MessageInfo> _queue;
		private ManualResetEventSlim _mres;

		public async Task EnqueueAsync(MessageInfo msgInfo)
		{
			_mres.Set();

			_queue.Enqueue(msgInfo);

			await msgInfo.TaskCompletionSource.Task;
		}
	}

	public class PrioritySend<TItem, TResult>
	{
		private class QueueItem
		{
			public TItem[] Items { get; set; }
			public TaskCompletionSource<TResult> Source { get; set; }
		}

		public PrioritySend(Func<TItem[], TResult> onItem)
		{
			_onItem = onItem;
			_msgQueue = new ConcurrentQueue<QueueItem>();
		}

		private readonly Func<TItem[], TResult> _onItem;
		private ConcurrentQueue<QueueItem> _msgQueue;

		public async Task Run(CancellationToken ct)
		{
			while (!ct.IsCancellationRequested)
			{
				if (_msgQueue.TryDequeue(out var result))
				{
					result.Source.SetResult(_onItem(result.Items));
				}
			}
		}

		public async Task SendAsync(params TItem[] msgs)
		{
			var qi = new QueueItem
			{
				Items = msgs,
				Source = new TaskCompletionSource<TResult>()
			};

			_msgQueue.Enqueue(qi);

			await qi.Source.Task;
		}
	}
}