﻿namespace genSendKeyPresses
{
	public abstract class ImplementationProvider
	{
		public abstract T Get<T>();
	}
}