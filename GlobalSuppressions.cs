﻿// This file is used by Code Analysis to maintain SuppressMessage
// attributes that are applied to this project.
// Project-level suppressions either have no target or are given
// a specific target and scoped to a namespace, type, member, etc.

[assembly: System.Diagnostics.CodeAnalysis.SuppressMessage("Style", "CC0061:Asynchronous method can be terminated with the 'Async' keyword.", Justification = "<Pending>", Scope = "member", Target = "~M:genSendKeyPresses.Program.Main(System.String[])~System.Threading.Tasks.Task")]