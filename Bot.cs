﻿using genSendKeyPresses.Services;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;

using PlayerIOClient.Fluent;

using System;
using System.Threading;
using System.Threading.Tasks;

namespace genSendKeyPresses
{
	public class Bot
	{
		public IConfigurationRoot Configuration { get; }

		public Bot()
		{
			Configuration = new ConfigurationBuilder()
				.SetBasePath(AppContext.BaseDirectory)
				.AddJsonFile("config.json")
				.Build();
		}

		public async Task Run(ImplementationProvider ip)
		{
			var services = new ServiceCollection();             // Create a new instance of a service collection
			ConfigureServices(services, ip);

			var provider = services.BuildServiceProvider();     // Build the service provider

			var ee = provider.GetService<EEService>();
			var mining = provider.GetService<MiningService>();
			var sender = provider.GetService<PrioritySend>();

			await ee.OnReady();

			Task.Run(async () =>
			{
				await sender.Run(new CancellationTokenSource().Token);
			});

			Task.Run(async () =>
			{
				await mining.DoWork(new CancellationTokenSource().Token);
			});
		}

		private void ConfigureServices(ServiceCollection services, ImplementationProvider ip)
		{
			services.AddSingleton(ip.Get<BlockDefinitions>())
				.AddSingleton(ip.Get<FluentClientWrapper>())
				.AddSingleton(ip.Get<FluentConnectionWrapper>())
				.AddSingleton(ip.Get<PrioritySend>())

				.AddSingleton<MiningService>()
				.AddSingleton<EEService>();
		}
	}
}