﻿using Microsoft.Extensions.Configuration;

using PlayerIOClient;
using PlayerIOClient.Fluent;

using System;
using System.Collections.Generic;
using Yonom.EE;

namespace genSendKeyPresses
{
	public class DefaultProvider : ImplementationProvider
	{
		private readonly IConfigurationRoot _config;

		public DefaultProvider(IConfigurationRoot config)
		{
			_data = new Dictionary<Type, object>();
			_config = config;
		}

		public void Provide()
		{
			// TODO: more defs
			var bd = new BlockDefinitions(100)
				.Add(Family.Gravity, 459, 0)
				.Add(Family.Dirt, 35, 1)
				.Add(Family.Dirt, 16, 1)
				.Add(Family.Dirt, 20, 7)
				.Add(Family.Dirt, 18, 5)
				.Add(Family.Dirt, 1023, 7)
				.Add(Family.Dirt, 1022, 5)
				.Add(Family.Lag, 44, 0)
				.Add(Family.Ore, 30, 3)
				.Add(Family.Ore, 1033, 5)
				.Add(Family.Ore, 1029, 9);

			var ps = new PrioritySend(async (m) =>
			{
				await m.SendAsync();
			});

			Console.WriteLine("Client");
			var client =
				PlayerIO.QuickConnect.SimpleConnect("everybody-edits-su9rn58o40itdbnw69plyw", _config["email"], _config["password"], null)
					.ApplyFluency();

			Console.WriteLine("BigDB Config");
			client
				.BigDB
					.Load("config", "config", out var eedbo);

			Console.WriteLine("Con");
			var con =
				client
					.Multiplayer
						.CreateJoinRoom(_config["world"], (_config["world"].StartsWith("BW") ? "Beta" : "Everybodyedits") + eedbo["version"], true, null, null);

			Console.WriteLine("Go");

			Store(bd);
			Store(ps);
			Store(client);
			Store(eedbo);
			Store(con);
		}

		private Dictionary<Type, object> _data;

		public override T Get<T>()
		{
			if (_data.TryGetValue(typeof(T), out var result))
			{
				return (T)result;
			}
			else
			{
				throw new Exception($"Unable to find {typeof(T)} in _data");
			}
		}

		private void Store<T>(T value)
			=> _data[typeof(T)] = value;

		private static void DoInitParse(Message m)
		{
			var tmpData = new uint[WorldData.Data.GetLength(0), WorldData.Data.GetLength(1), WorldData.Data.GetLength(2)];

			foreach (var chunk in InitParse.Parse(m))
			{
				foreach (var point in chunk.Locations)
				{
					tmpData[chunk.Layer, point.X, point.Y] = chunk.Type;
				}
			}

			WorldData.Data = tmpData;
		}
	}
}