﻿using PlayerIOClient;

using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace genSendKeyPresses
{
	internal class Program
	{
		private static async Task Main(string[] args)
		{
			var bot = new Bot();

			var provider = new DefaultProvider(bot.Configuration);
			provider.Provide();

			await bot.Run(provider);
			await Task.Delay(-1);

			/*
			uint[,,] rd = new uint[1,1,1];

			var bot = PlayerIO.QuickConnect.SimpleConnect("everybody-edits-su9rn58o40itdbnw69plyw", "redacted", "redacted", null)
	.ApplyFluency() //make the client fluent

	.BigDB
		.Load("config", "config", out var dbo)
		.Previous()

	.ConnectUserId(out var connectUserId)

	.Multiplayer
		.CreateJoinRoom("xx", "Everybodyedits" + dbo["version"], false, null, null);

			var ps = new PrioritySend<Message, bool>((m) =>
			{
				foreach (var i in m)
				{
					bot.Send(i);
					System.Threading.Thread.Sleep(20);
				}

				return true;
			});

			Task.Run(() =>
			{
				ps.Run(new CancellationTokenSource().Token).GetAwaiter().GetResult();
			});

			bot
				.OnDisconnect((c, i, e) => {
					if (i == DisconnectionType.Unexplained)
					{
						Console.WriteLine($"Disconnected for no reason: {e}");
						c.Reconnect()
						.Send("init");
					}
					else
					{
						Console.WriteLine($"Purposely disconnected");
					}
				})

				.On("init2", async (c, e) => {
					Console.WriteLine("init2");

					for (int y = 66; y <= 500; y++)
					{
						int bMine = 0;

						for (int x = 41; x <= 158; x += 3)
						{
							if (rd[0, x, y] == 44)
							{
								y -= 2;
							}

							if (rd[0, x, y] != 459 && rd[0, x, y] != 35)
							{
								int i = 50;

								if (rd[0, x, y] == 16)
								{
									i = 2;
								}

								if (rd[0, x, y] == 30)
								{
									i = 3;
								}

								if (rd[0, x, y] == 1033)
								{
									i = 5;
								}

								if (rd[0, x, y] == 1029)
								{
									i = 17;
								}

								if (rd[0, x, y] == 53)
								{
									i = 17;
								}

								if (rd[0, x, y] == 20)
								{
									i = 7;
								}

									bMine++;

									await ps.SendAsync(MessageHelpers.Mine(x, y).Repeat(i));

									await Task.Delay(50);
							}
						}

						if (bMine > 5)
						{
							await ps.SendAsync(
								MessageHelpers.Mine(194, 4).Repeat(10)
									.JoinMsgs(MessageHelpers.Mine(32, 60).Repeat(3))
								);

							await Task.Delay(1000);
						}
					}
				})

				.On("init", (c, e) => {
					Console.WriteLine("init");

					var roomData = new uint[2, e.GetInt(18), e.GetInt(19)];
					var chunks = InitParse.Parse(e);
					foreach (var chunk in chunks)
						foreach (var pos in chunk.Locations)
							roomData[chunk.Layer, pos.X, pos.Y] = chunk.Type;
					rd = roomData;

					c
						.Send("init2");
				})

				.On("reset", (c, e) => {
					Console.WriteLine("reset");

					var roomData = new uint[2, e.GetInt(18), e.GetInt(19)];
					var chunks = InitParse.Parse(e);
					foreach (var chunk in chunks)
						foreach (var pos in chunk.Locations)
							roomData[chunk.Layer, pos.X, pos.Y] = chunk.Type;

					rd = roomData;
				})

				.On("pm", (c, e) =>
				{
					Console.WriteLine(e);
				})

				.On("b", async (c, e) =>
				{
				var id = e.GetUInt(3);

					var x = e.GetInt(1);
					var y = e.GetInt(2);

					if (x < 41 || x > 158)
					{
					}
					else
					{
						rd[e.GetInt(0), x, y] = id;

						if (e.GetInt(0) == 0 && id != 459 && id != 16)
						{
							int reps = 100;

							if (y <= 100)
							{
								reps = 10;
							}

							if (y <= 150)
							{
								reps = 30;
							}
							Console.WriteLine($"Minimg {e}");
							ps.SendAsync(
								MessageHelpers.Mine(x, y).Repeat(reps)
								)
									.GetAwaiter().GetResult();
						}
					}
			})

			.Send("init");

			Console.ReadLine();
		*/
		}
	}

	public static class MessageHelpers
	{
		public static Message Mine(int x, int y)
			=> Message.Create("m", x * 16, (y - 1) * 16, 0, 0, 0, 0, 0, 1, 0, true, true, 0);

		public static Message[] Repeat(this Message msg, int count)
			=> Enumerable.Repeat(msg, count).ToArray();

		public static Message[] JoinMsgs(this Message msg, IEnumerable<Message> other)
			=> new[] { msg }.JoinMsgs(other);

		public static Message[] JoinMsgs(this Message[] msgs, IEnumerable<Message> other)
			=> msgs.Concat(other).ToArray();
	}
}