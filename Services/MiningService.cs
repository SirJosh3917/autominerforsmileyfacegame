﻿using PlayerIOClient;
using PlayerIOClient.Fluent;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace genSendKeyPresses.Services
{
	public class MiningService
	{
		private FluentConnectionWrapper _con;
		private PrioritySend _prioritySend;
		private BlockDefinitions _blockDefinitions;

		public MiningService(FluentConnectionWrapper con, PrioritySend prioritySend, BlockDefinitions blockDefinitions)
		{
			_con = con;
			_prioritySend = prioritySend;
			_blockDefinitions = blockDefinitions;
		}

		public async Task DoWork(CancellationToken ct)
		{
			while(!ct.IsCancellationRequested)
			{
				for(int y = 67; y < 225; y++)
				{
					int blocksMined = 0;

					int startX = 41;

					for (int x = startX; x < 158; x++)
					{
						var def = _blockDefinitions.Get(WorldData.Data[0, x, y]);
						
						if (def.Family == Family.Gravity)
						{
							continue;
						}

						if ((x - startX) % 3 == 0)
						{
							if (def.Family == Family.Lag)
							{
								y -= 2;
								break;
							}
						}
						else
						{
							continue;
						}

						var mi = GenMessage(def, x, y);

						await _prioritySend.EnqueueAsync(mi);

						blocksMined++;

						await Task.Delay(50);
					}

					if (blocksMined >= 10)
					{
						await SaveAndRepair();
					}

					if (ct.IsCancellationRequested) return;
				}
			}
		}

		public async Task OnBlock(B m)
		{
			if (m.Y >= 222 ||
				m.X < 40 || m.X > 159) return;

			var def = _blockDefinitions.Get(m.Id);

			if(UselessBlock(def))
			{
				return;
			}

			var mi = GenMessage(def, (int)m.X, (int)m.Y);

			await _prioritySend.EnqueueAsync(mi);
		}

		private MessageInfo GenMessage(Block def, int x, int y)
			=> new MessageInfo(_con.Connection,
				MessageHelpers.Mine(x, y)
				.Repeat(def.MineAttempts + 3))
			{
				Log = $"Mining \"{def}\""
			};

		private async Task SaveAndRepair()
		{
			var mi = new MessageInfo(_con.Connection,
				MessageHelpers.Mine(194, 5).Repeat(10));

			await _prioritySend.EnqueueAsync(mi);

			await Task.Delay(1000);

			var mi2 = new MessageInfo(_con.Connection,
				MessageHelpers.Mine(32, 61).Repeat(3)
			);

			await _prioritySend.EnqueueAsync(mi2);

			await Task.Delay(1000);
		}

		private bool UselessBlock(Block def)
			=> def.Family == Family.Dirt ||
				def.Family == Family.Gravity;
	}
}
