﻿using PlayerIOClient;
using PlayerIOClient.Fluent;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Yonom.EE;

namespace genSendKeyPresses.Services
{
	public class EEService
	{
		private FluentConnectionWrapper _con;
		private MiningService _miningService;

		private TaskCompletionSource<bool> _tcs;

		public EEService(FluentConnectionWrapper con, MiningService miningService)
		{
			_tcs = new TaskCompletionSource<bool>();
			_con = con;
			_miningService = miningService;

			Console.WriteLine("Attatching Logic");
			AttatchBasicLogic();

			Console.WriteLine("Init...");
			con.Send("init");
			Console.WriteLine("Sent!");

			void AttatchBasicLogic()
			{
				con.OnDisconnect((c, i, e) =>
				{
					if (i == DisconnectionType.Unexplained)
					{
						Console.WriteLine($"Disconnected for no reason: {e}");
						c.Reconnect()
						.Send("init");
					}
					else
					{
						Console.WriteLine($"Purposely disconnected");
					}
				});

				con
					.On("init", (c, m) =>
					{
						Console.WriteLine("Rec Init");

						var msg = m.Deserialize<Init>();

						WorldData.Data = new uint[2, msg.Width, msg.Height];

						DoInitParse(m);

						c.Send("init2");
					})

					.On("init2", (c, m) =>
					{
						_tcs.SetResult(true);
					})

					.On("reset", (c, m) =>
					{
						DoInitParse(m);
					})

					.On("pm", (c, m) => Console.WriteLine(m))

					.On("b", async (c, m) =>
					{
						var b = m.Deserialize<B>();

						WorldData.Data[b.Layer, b.X, b.Y] = b.Id;

						await _miningService.OnBlock(b);
					});
			}

			void DoInitParse(Message m)
			{
				var tmpData = new uint[WorldData.Data.GetLength(0), WorldData.Data.GetLength(1), WorldData.Data.GetLength(2)];

				foreach (var chunk in InitParse.Parse(m))
				{
					foreach (var point in chunk.Locations)
					{
						tmpData[chunk.Layer, point.X, point.Y] = chunk.Type;
					}
				}

				WorldData.Data = tmpData;
			}
		}

		public async Task OnReady()
		{
			await _tcs.Task;
		}
	}
}
