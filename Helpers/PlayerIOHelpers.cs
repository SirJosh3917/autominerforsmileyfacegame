﻿using Decorator;
using PlayerIOClient;
using System;
using System.Collections.Generic;
using System.Text;

namespace genSendKeyPresses
{
	public static class PlayerIOHelpers
	{
		public static object[] GetAsArray(this Message m)
		{
			var data = new object[m.Count];

			for (var i = 0u; i < m.Count; i++)
				data[i] = m[i];

			return data;
		}

		public static T Deserialize<T>(this Message m) where T : IDecorable, new()
		{
			if (!DConverter<T>.TryDeserialize(m.GetAsArray(), out var msg))
			{
				throw new Exception($"EE Updated Protocol: '{m.Type}'");
			}

			return msg;
		}
	}
}
