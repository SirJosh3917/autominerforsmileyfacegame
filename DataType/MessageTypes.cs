﻿using Decorator;

namespace genSendKeyPresses
{
	public class Init : IDecorable
	{
		[Position(18), Required]
		public int Width { get; set; }

		[Position(19), Required]
		public int Height { get; set; }
	}

	public class B : IDecorable
	{
		[Position(0), Required]
		public int Layer { get; set; }

		[Position(1), Required]
		public uint X { get; set; }

		[Position(2), Required]
		public uint Y { get; set; }

		[Position(3), Required]
		public uint Id { get; set; }

		[Position(4), Required]
		public uint PlayerId { get; set; }
	}
}