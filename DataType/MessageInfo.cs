﻿using PlayerIOClient;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace genSendKeyPresses
{
	public class MessageInfo
	{
		public MessageInfo(Connection sender, params Message[] blocks) : this(sender, (IEnumerable<Message>)blocks)
		{
		}

		public MessageInfo(Connection sender, IEnumerable<Message> blocks)
		{
			Sender = sender;
			Messages = blocks.ToArray();
			CancellationTokenSource = new CancellationTokenSource();
			TaskCompletionSource = new TaskCompletionSource<bool>();
		}

		public string Log { get; set; } = "";

		public Connection Sender { get; }
		public Message[] Messages { get; }
		public CancellationTokenSource CancellationTokenSource { get; }
		public TaskCompletionSource<bool> TaskCompletionSource { get; }

		public async Task SendAsync()
		{
			if(!string.IsNullOrWhiteSpace(Log))
			{
				Console.WriteLine($"{Log}");
			}

			foreach (var i in Messages)
			{
				if (CancellationTokenSource.Token.IsCancellationRequested)
				{
					TaskCompletionSource.SetResult(false);
					return;
				}
				
				Sender.Send(i);

				// TODO: make this easier to change
				await Task.Delay(20);
			}

			TaskCompletionSource.SetResult(true);
		}
	}
}